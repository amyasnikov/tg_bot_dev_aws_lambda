module gitlab.com/amyasnikov/aws_golang_sample

go 1.16

require (
	github.com/aws/aws-lambda-go v1.24.0
	gopkg.in/tucnak/telebot.v2 v2.3.5
)
