package main

import (
	"testing"

	"github.com/aws/aws-lambda-go/events"
)

func TestOne(t *testing.T) {
	request := events.APIGatewayProxyRequest{
		Body: "Paul",
	}

	response, err := HandlerTrace(request)
	if err != nil {
		t.Error(err)
	}

	t.Log(response.Body)
	t.Log(response.StatusCode)
}

func TestParse(t *testing.T) {
	request := events.APIGatewayProxyRequest{
		Body: `{
      "update_id": 146961391,
      "message": {
        "message_id": 1670,
        "from": {
          "id": 186069298,
          "is_bot": false,
          "first_name": "Alexander",
          "last_name": "Myasnikov",
          "username": "amyasnikov",
          "language_code": "en"
        },
        "chat": {
          "id": 186069298,
          "first_name": "Alexander",
          "last_name": "Myasnikov",
          "username": "amyasnikov",
          "type": "private"
        },
        "date": 1622916149,
        "text": "simple message for testing"
      }
    }`,
	}

	response, err := HandlerTg(request)
	if err != nil {
		t.Error(err)
	}

	if response.StatusCode != 200 {
		t.Error(response.StatusCode)
	}

	t.Log(response.Body)
}
