package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	tb "gopkg.in/tucnak/telebot.v2"
)

func HandlerTrace(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	data, err := json.MarshalIndent(request, "", "  ")
	if err != nil {
		fmt.Printf("Body: %s \n", err.Error())
		return events.APIGatewayProxyResponse{
			Body:       err.Error(),
			StatusCode: 500,
		}, nil
	}

	fmt.Printf("Body: %s \n", string(data))

	return events.APIGatewayProxyResponse{
		Body:       string(data),
		StatusCode: 200,
	}, nil
}

func HandlerTg(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	if data, err := json.MarshalIndent(request, "", "  "); err != nil {
		fmt.Printf("request: %s \n", string(data))
	}

	fmt.Printf("Body: %s \n", request.Body)

	token := os.Getenv("TGTOKEN")
	if token == "" {
		return events.APIGatewayProxyResponse{StatusCode: 400, Body: `{"ok":false,"error_message":"empty TGTOKEN"}`}, nil
	}

	update := tb.Update{}
	if err := json.Unmarshal([]byte(request.Body), &update); err != nil {
		fmt.Printf("error: %v \n", err)
		return events.APIGatewayProxyResponse{StatusCode: 400, Body: `{"ok":false,"error_message":"can not parse body"}`}, nil
	}

	if update.Message == nil {
		return events.APIGatewayProxyResponse{StatusCode: 400, Body: `{"ok":false,"error_message":"empty messages"}`}, nil
	}

	message := update.Message
	fmt.Printf("text: %v \n", message.Text)
	text := "Hello, " + message.Chat.FirstName + ". You send message: " + message.Text

	bot, err := tb.NewBot(tb.Settings{Token: token})
	if err != nil {
		return events.APIGatewayProxyResponse{StatusCode: 400, Body: `{"ok":false,"error_message":"can not create bot"}`}, nil
	}

	bot.Send(message.Chat, text)

	return events.APIGatewayProxyResponse{StatusCode: 200, Body: `{"ok":true}`}, nil
}

func main() {
	lambda.Start(HandlerTg)
}
